<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 256);
            $table->string('searchname', 256)->unique();
            $table->unsignedBigInteger('country_id');
            $table->string('info',1000);
            $table->unsignedInteger('rating')->default(0);
            $table->unsignedInteger('number_of_votes')->default(0);
            $table->timestamps();

            $table->unique(['id', 'country_id']);
            $table->index('name');
            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
