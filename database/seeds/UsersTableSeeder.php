<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'mario',
            'email' => 'helpereiden@gmail.com',
            'password' => bcrypt('mario123'),
            'is_admin' => 1
        ]);


        User::insert([
            [
                'username' => 'test',
                'email' => 'helpereiden1@gmail.com',
                'password' => bcrypt('mario123'),
                'is_admin' => 0
            ],
            [
                'username' => 'ivan',
                'email' => 'helpereiden2@gmail.com',
                'password' => bcrypt('mario123'),
                'is_admin' => 0
            ],
            [
                'username' => 'mario1',
                'email' => 'helpereiden3@gmail.com',
                'password' => bcrypt('mario123'),
                'is_admin' => 1
            ],
            [
                'username' => 'ante',
                'email' => 'helpereiden4@gmail.com',
                'password' => bcrypt('mario123'),
                'is_admin' => 0
            ],
            [
                'username' => 'test 2',
                'email' => 'helpereiden5@gmail.com',
                'password' => bcrypt('mario123'),
                'is_admin' => 1
            ]
        ]);
    }
}
