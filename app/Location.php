<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Location extends Model
{

    protected $fillable = [
        'name',
        'searchname',
        'rating',
        'number_of_votes',
        'info'
    ];

    protected $hidden = [
        'searchname'
    ];


    protected $casts = [
        'rating' => 'integer'
    ];
    /**
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function reviews() : HasMany
    {
        return $this->hasMany(Review::class);
    }
}
