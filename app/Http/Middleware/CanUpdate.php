<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanUpdate
{

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::id() !== $request->user->id && !Auth::user()->is_admin) {
            return response()->json([
                'success' => false,
                'message' => 'You don\'t have permissions for this action.'
            ], 403);
        }

        if(count($request->all()) > 1 && Auth::id() !== $request->user->id ){
            return response()->json([
                'success' => false,
                'message' => 'You can\'t change other users info except for admin property.'
            ], 403);
        }

        return $next($request);

    }
}
