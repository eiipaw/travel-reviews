<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsOwner
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->review->user_id !== Auth::user()->id) {
            return response()->json([
                'success' => false,
                'message' => "You're not the owner or it doesn't exist."
            ], 403);
        }
        return $next($request);
    }
}
