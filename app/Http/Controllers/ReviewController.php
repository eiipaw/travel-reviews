<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Location;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    /**
     * @param int $locationId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $locationId)
    {
        $comments = Review::with('user')
            ->where('location_id', $locationId)
            ->paginate(3);
        return response()->json($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param ReviewRequest $request
     * @param int           $locationId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReviewRequest $request, int $locationId)
    {

        $reviewData = $request->all();
        $reviewData['user_id'] = Auth::id();
        $reviewData['location_id'] = $locationId;
        $location = Location::findOrFail($locationId);
        $location->increment('number_of_votes');
        $location->increment('rating', $request->rating);
        $review = Review::create($reviewData);
        $review->load('user');
        return response()->json([
            'review' => $review,
            'location' => $review->location->load('country')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * @param ReviewRequest $request
     * @param Review        $review
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReviewRequest $request, Review $review)
    {
        $result = $review->update($request->all());

        $updatedPrice = Review::where('location_id', $review->location_id)->sum('rating');
        $review->location->update([
            'rating' => $updatedPrice
        ]);
        return response()->json([
            'success' => (bool)$result,
            'message' => $result ? "Successfully updated." : "There was a problem.",
            'data' => $review->location->load('country')
        ]);
    }

    /**
     * @param Review $review
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Review $review)
    {
        $rating = $review->rating;
        $location = $review->location;
        $result = $review->delete();

        if ($result) {
            $location->decrement('number_of_votes');
            $location->decrement('rating', $rating);
        }

        return response()->json([
            'success' => (bool)$result,
            'message' => $result ? "Successfully deleted." : "There was a problem.",
            'data' => $review->location->load('country')
        ]);
    }
}
