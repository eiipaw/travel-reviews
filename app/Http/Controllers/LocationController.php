<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\StoreLocationRequest;
use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * @param StoreLocationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreLocationRequest $request)
    {

        $country = Country::where('name', '=', $request->country)->first();
        if (!$country) {
            $country = Country::create([
                'name' => $request->country,
                'short' => strtoupper(substr($request->country, 0, 3))
            ]);

        }
        $location = $country->locations()->create([
            'name' => $request->location,
            'searchname' => strtolower(preg_replace("/[^A-Za-z0-9]/", '', $request->location . $request->country)),
            'info' => $request->info
        ]);

        $location->load('country');

        return response()->json($location);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Location $destination
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Location $destination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Location            $destination
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $destination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Location $destination
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $destination)
    {
        //
    }
}
