<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Http\Requests\UserRequest;
use App\Location;
use App\Review;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        return view('admin', ['isAdminPage' => true]);
    }

    public function all(Request $request)
    {
        $username = $request->q;
        $users = User::where([
            ['username', 'LIKE', "%$username%"],
            ['id', '!=', Auth::id()]
        ])->paginate(4);

        return response()->json($users);
    }

    /**
     * @param ReviewRequest $request
     * @param int           $locationId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReviewRequest $request, int $locationId)
    {


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Review $review
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * @param UserRequest $request
     * @param User        $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if(count($request->all())>1){
            $data = $request->validate([
                'username' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
                'password_confirmation' => ['required_with:password', 'string', 'min:8', 'same:password'],
            ]);
            $data['password'] = bcrypt($data["password_confirmation"]);
            unset($data['password_confirmation']);
        }else{
            $data = $request->validate([
                'is_admin' => ['required', 'boolean'],
            ]);
        }

        foreach ($data as $key => $value) {
            $user->{$key} = $value;
        }
        $result = $user->save();
        return response()->json([
            'success' => (bool)$result,
            'message' => $result ? "Successfully updated." : "There was a problem."
        ]);
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $result = $user->delete();

        return response()->json([
            'success' => (bool)$result,
            'message' => $result ? "Successfully deleted." : "There was a problem."
        ]);
    }
}
