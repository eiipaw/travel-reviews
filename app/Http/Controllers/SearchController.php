<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Location;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * @param SearchRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchDestination(SearchRequest $request)
    {
        $search = strtolower(preg_replace("/[^A-Za-z0-9]/", '%', $request->q));
        $results = Location::select("*", DB::raw("AVG(rating/number_of_votes) AS average"))
            ->with('country')
            ->where('searchname', 'LIKE', "%$search%")
            ->groupBy('searchname')
            ->get();
        return response()->json(['results' => $results]);
    }
}
