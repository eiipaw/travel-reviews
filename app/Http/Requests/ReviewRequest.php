<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'bail|required|digits:1|digits_between:1,5',
            'pros' => 'required|min:5|max:400',
            'cons' => 'required|min:5|max:400',
            'comment' => 'required|min:5|max:1000',
        ];
    }
}
