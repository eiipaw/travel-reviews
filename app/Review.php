<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Review extends Model
{

    protected $fillable = [
        'rating',
        'user_id',
        'location_id',
        'pros',
        'cons',
        'comment'
    ];

    public function location() :BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
