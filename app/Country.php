<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    protected $fillable= [
        'name',
        'short'
    ];

    public function locations() : HasMany
    {
        return $this->hasMany(Location::class);
    }
}
