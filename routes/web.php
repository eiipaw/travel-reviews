<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('search', 'SearchController@searchDestination');

Route::get('location/{id}/comments', 'ReviewController@index');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::middleware(['auth.masteradmin'])->group(function () {
        Route::get('admin', 'UserController@index');
        Route::get('users', 'UserController@all');
        Route::delete('user/{user}', 'UserController@destroy');
        Route::patch('user/{user}', 'UserController@update');
    });

    Route::middleware(['canupdate'])->group(function () {
        Route::patch('user/{user}', 'UserController@update');
    });

    Route::post('locations', 'LocationController@store');
    Route::post('location/{id}/review', 'ReviewController@store');


    Route::middleware(['owner'])->group(function () {
        Route::put('review/{review}', 'ReviewController@update');
        Route::delete('review/{review}', 'ReviewController@destroy');
    });
});

