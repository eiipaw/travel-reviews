import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        isAddLocation: false,
        modalOpen: false,
        errors: {
            searchMessage: ""
        },
        selectedLocation: null,
        selectedComment: null,
        selectedUser: null,
        searchQuery: "",
        inputRef: null,
        loginModal: false,
        loggedIn: false,
        user: null,
        comments: [],
        users: [],
        modal: {
            local: {
                opened: false,
            },
            global: {
                opened: false,
            }
        }
    },
    mutations: {
        setAddLocation(state, value) {
            state.isAddLocation = value;
        },
        setModalOpen(state, value) {
            state.modalOpen = value;
        },
        setErrorSearchMessage(state, value) {
            state.errors.searchMessage = value;
        },
        setSelectedLocation(state, value) {
            value.average = value.rating / value.number_of_votes;
            state.selectedLocation = value;
        },
        setSelectedComment(state, value) {
            state.selectedComment = value;
        },
        setSelectedUser(state, value) {
            state.selectedUser = value;
        },
        setUser(state, value) {
            state.user = value;
            state.loggedIn = Boolean(value);

        },
        setModal(state, payload) {
            state.modal[payload[0]][payload[1]] = payload[2];

        },
        // setLoggedIn(state, value) {
        //     state.loggedIn = value;
        // },
        setLoginModal(state, value) {
            state.loginModal = value;
        },
        setInputRef(state, value) {
            state.inputRef = value;
        },
        setComments(state, value) {
            state.comments = Object.values(value);
        },
        setUsers(state, value) {
            state.users = Object.values(value);
        },
        setSearchQuery(state, value) {
            if (this.state.isAddLocation) this.state.isAddLocation = false;
            if (this.state.isAddReview) this.state.isAddReview = false;
            if (this.state.selectedLocation) this.state.selectedLocation = null;
            state.searchQuery = value;
        },
        updateArrayValue(state, payload) {
            state[payload[0]] = state[payload[0]].map(item => {
                if (item.id === payload[1]) {
                    if (payload[2] === null) {
                        return null;
                    } else {
                        return Object.assign({}, payload[2]);
                    }
                } else {
                    return item;
                }
            }).filter(item => {
                return (item !== null);
            });
        },
        pushToArray(state, payload) {
            state[payload[0]].push(payload[1]);
        },
    },
    getters: {
        getAddLocation(state) {
            return state.isAddLocation;
        },
        getModalOpen(state) {
            return state.modalOpen;
        },
        getComments(state) {
            return state.comments;
        },
        getUsers(state) {
            return state.users;
        },
        getErrorSearchMessage(state) {
            return state.errors.searchMessage;
        },
        getSelectedLocation(state) {
            return state.selectedLocation;
        },
        getSelectedComment(state) {
            return state.selectedComment;
        },
        getSelectedUser(state) {
            return state.selectedUser;
        },
        getUser(state) {
            return state.user;
        },
        getSearchQuery(state) {
            return state.searchQuery;
        },
        getInputRef(state) {
            return state.inputRef;
        },
        getLoggedIn(state) {
            return state.loggedIn;
        },
        getLoginModal(state) {
            return state.loginModal;
        },
        getModal: (state) => (modalType) => {
            return state.modal[modalType];
        },
    }
});