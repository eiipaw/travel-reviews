@extends('layouts.app')

{{--@yield('title', 'Travel reviews')--}}

@section('body')
    <user-search></user-search>
    <userstack></userstack>
    <user-delete-modal></user-delete-modal>
    <user-edit-modal></user-edit-modal>
@stop

@section('scripts')
    <script>
        var user = @json(Auth::user());
        var isadmin = @json($isAdminPage);
    </script>
    <script src="{{ asset('js/admin.js') }}"></script>
@stop
