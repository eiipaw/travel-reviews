<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield ('title')</title>
    <meta charset="utf-8">
    <meta lang="en">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @section('styles')
    @show
</head>

<body>
<div id="app">
    @section('body')
    @show
</div>
@section('scripts')
@show
</body>
</html>