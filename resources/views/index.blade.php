@extends('layouts.app')

{{--@yield('title', 'Travel reviews')--}}

@section('body')
    <search></search>
    <userstack></userstack>

@stop

@section('scripts')
    <script>
        var user = @json(Auth::user());
        var isadmin = @json($isAdminPage ?? false);

    </script>
    <script src="{{ asset('js/app.js') }}"></script>
@stop

